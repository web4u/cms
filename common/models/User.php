<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public $password;
    public $password_new;
    public $password_repeat;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    static $pageName = 'Пользователи';

    public function attributeLabels()
    {
        return [
            'text' => 'Описание',
            'password' => 'Пароль',
            'password_new' => 'Новый пароль',
            'password_repeat' => 'Новый пароль еще раз', 'name' => 'Имя',
            'lastname' => 'Фамилия',
            'email' => 'Email',
            'phone' => 'Телефон',
            'role' => 'Роль',
            'category' => 'Специализация',
        ];
    }

    public function rows()
    {
        return [
            [
                'table' => [
                    'field' => 'id',
                    'title' => 'ID',
                    'sortable' => false,
                    'width' => 40,
                    'selector' => ['class' => 'm-checkbox--solid m-checkbox--brand'],
                    'textAlign' => 'center'
                ],
                'view' => [
                    'filter' => false,
                    'type' => 'none',
                    'display' => true
                ]
            ],
            [
                'table' => [
                    'field' => 'email',
                    'title' => 'Email',
                    'sortable' => true,
                    'selector' => false,
                    'textAlign' => 'left'
                ],
                'view' => [
                    'filter' => false,
                    'type' => 'input',
                    'display' => true
                ]
            ],
            [
                'table' => [
                    'field' => 'name',
                    'title' => 'Имя',
                    'sortable' => true,
                    'selector' => false,
                    'textAlign' => 'left'
                ],
                'view' => [
                    'filter' => false,
                    'type' => 'input',
                    'display' => true
                ]
            ],
            [
                'table' => [
                    'field' => 'lastname',
                    'title' => 'Фамилия',
                    'sortable' => true,
                    'selector' => false,
                    'textAlign' => 'left'
                ],
                'view' => [
                    'filter' => false,
                    'type' => 'input',
                    'display' => true
                ]
            ],
            [
                'table' => [
                    'field' => 'phone',
                    'title' => 'Телефон',
                    'sortable' => true,
                    'selector' => false,
                    'textAlign' => 'left'
                ],
                'view' => [
                    'filter' => false,
                    'type' => 'input',
                    'display' => true,
                    'attr' => [
                        'disabled' => true
                    ]
                ]
            ],
            [
                'table' => [
                    'field' => 'role',
                    'title' => 'Роль',
                    'sortable' => true,
                    'selector' => false,
                    'textAlign' => 'left'
                ],
                'view' => [
                    'filter' => true,
                    'type' => 'radio',
                    'display' => true,
                    'data' => [
                        0 => 'заказчик',
                        1 => 'исполнитель'
                    ]
                ]
            ],
            [
                'table' => [
                    'field' => 'category',
                    'title' => 'Специализация',
                    'sortable' => true,
                    'selector' => false,
                    'textAlign' => 'left'
                ],
                'view' => [
                    'filter' => false,
                    'type' => 'input',
                    'display' => false,
                ]
            ],
            [
                'table' => [
                    'field' => 'text',
                    'title' => 'Описание',
                    'sortable' => true,
                    'selector' => false,
                    'textAlign' => 'left'
                ],
                'view' => [
                    'filter' => false,
                    'type' => 'textarea',
                    'display' => false,
                ]
            ],
            [
                'table' => [
                    'field' => 'status',
                    'title' => 'Состояние',
                    'sortable' => true,
                    'selector' => false,
                    'textAlign' => 'left'
                ],
                'view' => [
                    'filter' => true,
                    'type' => 'radio',
                    'display' => true,
                    'data' => [
                        0 => 'не активный',
                        10 => 'активный'
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['password_new', 'string', 'min' => 6],
            ['password_repeat', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute' => 'password_new'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['phone' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
