<?php

use yii\db\Migration;

/**
 * Class m171108_172355_create_table_admin
 */
class m171108_172355_create_table_admin extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
	$this->createTable('admin', [
	    'id'=>$this->primaryKey(),
	    'username'=>$this->string(255),
	    'auth_key'=>$this->string(32),
	    'password_hash'=>$this->string(255),
	    'password_reset_token'=>$this->string(255),
	    'email'=>$this->string(255),
	    'status'=>$this->integer(6),
	    'created_at'=>$this->integer(11),
	    'updated_at'=>$this->integer(11),
	    'name'=>$this->string(100),
	    'role'=>$this->integer(11),
	    'photo'=>$this->string(500)
	]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171108_172355_create_table_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171108_172355_create_table_admin cannot be reverted.\n";

        return false;
    }
    */
}
