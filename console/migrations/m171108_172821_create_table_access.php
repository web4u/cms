<?php

use yii\db\Migration;

/**
 * Class m171108_172821_create_table_access
 */
class m171108_172821_create_table_access extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
	$this->createTable('access',[
	    'id'=>$this->primaryKey(),
	    'name'=>$this->string(100),
	    'access'=>$this->string(500)
	]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171108_172821_create_table_access cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171108_172821_create_table_access cannot be reverted.\n";

        return false;
    }
    */
}
