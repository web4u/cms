<?php

use yii\db\Migration;

class m171105_150300_create_table_logs extends Migration
{
    public function safeUp()
    {
	$this->createTable('logs',[
	    'id'=>$this->primaryKey(),
	    'data'=>$this->date(),
	    'time'=>$this->time(),
	    'command'=>$this->string(200),
	    'table'=>$this->string(100),
	    'text'=>$this->text(),
	    'text_old'=>$this->text(),
	    'user'=>$this->string(100)
	]);
    }

    public function safeDown()
    {
        echo "m171105_150300_create_table_logs cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171105_150300_create_table_logs cannot be reverted.\n";

        return false;
    }
    */
}
