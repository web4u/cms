<?php

use yii\db\Migration;

/**
 * Class m171108_154005_change_table_user
 */
class m171108_154005_change_table_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
	$this->dropColumn('user', 'username');
	$this->addColumn('user', 'name', $this->string(100));
	$this->addColumn('user', 'lastname', $this->string(100));
	$this->addColumn('user', 'phone', $this->string(20));
	$this->addColumn('user', 'role', $this->integer(1));
	$this->addColumn('user', 'category', $this->string(200));
	$this->addColumn('user', 'text', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171108_154005_change_table_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171108_154005_change_table_user cannot be reverted.\n";

        return false;
    }
    */
}
