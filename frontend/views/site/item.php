<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = $user['name'].' '.$user['lastname'];
$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['/catalog']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-info">
        <p>
            <strong>Email:</strong> <?php echo $user['email'] ?>
        </p>
        <p>
            <strong>Телефон:</strong> <?php echo $user['phone'] ?>
        </p>
        <p>
            <strong>Описание:</strong> <?php echo $user['text']!=''?$user['text']:'не заполнено' ?>
        </p>
        <p>
            <strong>Специализация:</strong> <?php echo $user['category'] ?>
        </p>
    </div>
</div>
