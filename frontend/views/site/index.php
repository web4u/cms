<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Добро пожаловать</h1>

        <p class="lead">Это главная страница сайта</p>

        <p><a class="btn btn-lg btn-success" href="<?php echo \yii\helpers\Url::to('catalog');?>">Перейти в каталог</a></p>
    </div>
</div>
