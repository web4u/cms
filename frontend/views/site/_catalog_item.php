<a href="<?php echo $url ?>" class="alert alert-info catalog-item">
    <p>
        <strong>ФИО:</strong> <?php echo $name ?>
    </p>
    <p>
        <strong>Описание:</strong> <?php echo $text ?>
    </p>
    <p>
        <strong>Специализация:</strong> <?php echo $category ?>
    </p>
</a>