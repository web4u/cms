<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <h3>Основные данные</h3>
            <?php $form = ActiveForm::begin(['id' => 'profile']); ?>
            <?= $form->field($model, 'role')->dropDownList([0 => 'Заказчик', 1 => 'Исполнитель'], ['id' => 'role']); ?>
            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'lastname')->textInput() ?>

            <?= $form->field($model, 'email')->textInput() ?>
            <?= $form->field($model, 'phone')->textInput() ?>

            <?
            if (Yii::$app->user->identity->role == 1) {
                echo $form->field($model, 'category')->textInput();
                echo $form->field($model, 'text')->textarea();
            }
            ?>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-5 col-lg-offset-1">
            <h3>Смена пароль</h3>
            <?php $form = ActiveForm::begin(['id' => 'password-form']); ?>

            <?= $form->field($model, 'password_new')->passwordInput() ?>
            <?= $form->field($model, 'password_repeat')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Изменить', ['class' => 'btn btn-primary', 'name' => 'pass-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
