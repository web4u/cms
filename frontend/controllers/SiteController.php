<?php

namespace frontend\controllers;

use common\models\User;
use frontend\models\Helper;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays catalog page.
     *
     * @return mixed
     */
    public function actionCatalog()
    {
        $html = '';
        $users = User::find()->where(['role' => 1, 'status' => 10])->orderBy('id desc')->all();
        foreach ($users as $user) {
            $html .= $this->renderPartial('_catalog_item', [
                'name' => $user['name'] . ' ' . $user['lastname'],
                'text' => $user['text'] == '' ? 'не заполнено' : $user['text'],
                'category' => $user['category'],
                'url' => Url::to('catalog/' . Helper::pref($user['id'], $user['name'] . ' ' . $user['lastname']))
            ]);
        }
        return $this->render('catalog', [
            'catalog' => $html
        ]);
    }

    /**
     * Displays item page.
     *
     * @return mixed
     */
    public function actionItem($pref)
    {
        $tmp = explode('-', $pref);
        $id = array_pop($tmp);
        $users = User::findOne($id);
        return $this->render('item', [
            'user' => $users
        ]);
    }

    /**
     * Displays profile page.
     *
     * @return mixed
     */
    public function actionMy()
    {
        $model = User::findOne(Yii::$app->user->identity->id);
        if (Yii::$app->request->post()) {
            if (isset($_POST['save-button'])) {
                $model->role = Yii::$app->request->post('User')['role'];
                $model->name = Yii::$app->request->post('User')['name'];
                $model->lastname = Yii::$app->request->post('User')['lastname'];
                $model->email = Yii::$app->request->post('User')['email'];
                $model->phone = Yii::$app->request->post('User')['phone'];
                if (Yii::$app->user->identity->role == 1) {
                    $model->category = Yii::$app->request->post('User')['category'];
                    $model->text = Yii::$app->request->post('User')['text'];
                }
                if ($model->save()) {
                    Yii::$app->user->login($model, 3600 * 24 * 30);
                    Yii::$app->session->setFlash('success', 'Данные сохранены');
                } else {
                    Yii::$app->session->setFlash('error', 'Ошибка сохранения данных');
                }
            } else {
                $model->setPassword(Yii::$app->request->post('User')['password_new']);
                $model->generateAuthKey();

                if ($model->save()) {
                    Yii::$app->user->login($model, 3600 * 24 * 30);
                    Yii::$app->session->setFlash('success', 'Пароль изменен');
                } else {
                    Yii::$app->session->setFlash('error', 'Ошибка смены пароля');
                    print_r($model->getErrors());
                }
            }
        }
        return $this->render('my', [
            'model' => $model
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
