<?php

namespace frontend\models;

use backend\models\Kurs;
use backend\models\Lang;
use backend\models\News;
use backend\models\Blokitems;
use backend\models\Reviews;
use Yii;
use yii\base\Model;
use TelegramBot;
use yii\helpers\Url;

/**
 * ContactForm is the model behind the contact form.
 */
class Helper extends Model
{
    public function getShortText($text, $lenght){
        $text=strip_tags($text);
        if(mb_strlen($text, 'UTF-8')>$lenght)
        {
            $text = mb_substr($text, 0, $lenght, 'UTF-8');
            return $text.'...';
        }
        else
            return $text;
    }

    public function getMonth($month){
        $arr=[
            1 => "Январь",
            2 => "Февраль",
            3 => "Март",
            4 => "Апрель",
            5 => "Май",
            6 => "Июль",
            7 => "Июнь",
            8 => "Август",
            9 => "Сентябрь",
            10 => "Октябрь",
            11 => "Ноябрь",
            12 => "Декабрь",
        ];

        return $arr[$month];
    }

    public function getPicture($pic, $all=true, $default=false){
        $img=json_decode($pic);
        if (empty($img[0])){
            return $default==false?false:'/img/person.png';
        }
        return $all==true?$img:$img[0];
    }

    public function rus2translit($string) {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );
        return strtr($string, $converter);
    }
    public function str2url($str) {
        // переводим в транслит
        $str = self::rus2translit($str);
        // в нижний регистр
        $str = strtolower($str);
        // заменям все ненужное нам на "-"
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        // удаляем начальные и конечные '-'
        $str = trim($str, "-");
        return $str;
    }

    /**
     * TODO Пагинация
     * $active - активна сторынка
     * $table - таблиця
     * $count - к-сть по замовчуванню
     * $where - додатковы умови
     * $link - посилання
     **/
    public function paging($active, $table='Goods', $count=6, $where=[], $link='/'){
        if ($table=='User'){
            $class='\frontend\models\\'.$table;
        } else {
            $class='\backend\models\\'.$table;
        }
        $all=$class::find()->where($where)->count();
        $page=ceil($all/$count);
        $prev=$active==1?1:$active-1;
        $next=$active==$page?$page:$active+1;
        $html='<nav class="paginator">';
        $last=$page>10?10:$page;
        $first=1;
        if ($active>5){
            $first=$active-5;
            if ($active+5>$page){
                $last=$page;
            } else {
                $last=$active+4;
            }
        }

        if (substr_count($link, '?')>0){
            $link=$link.'&page=';
        } else {
            $link=$link.'?page=';
        }

        for ($i=$first;$i<=$last;$i++){
            if ($i==$active) {
                $html .= '<a class="active" href="'.$link.$i.'">'.$i.'</a>';
            } else {
                $html.='<a href="'.$link.$i.'">'.$i.'</a>';
            }
        }

        $html.='</nav>';

        return $html;
    }

    public function pref($id, $name){
        return self::str2url($name).'-'.$id;
    }

    public function vdm($number, $after) {
        $cases = array (2, 0, 1, 1, 1, 2);
        return $number.' '.$after[ ($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)] ];
    }

}
